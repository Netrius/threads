package Netriuscode.company.application.view;

import Netriuscode.company.application.controller.AppController;

import javax.swing.*;
import java.awt.*;

public class View {
    private AppController appController;
    private JFrame frame;
    private JPanel mainPanel;
    private JLabel userInfo;
    public void setAppController(AppController appController) {
        this.appController = appController;
    }

    public void showWindow(){
        frame=new JFrame("Testownik");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setMaximumSize(new Dimension(300,300));
        frame.setPreferredSize(new Dimension(500,500));


        //dodanie do mainPanelu
        mainPanel=new JPanel();
        mainPanel.setLayout(new BoxLayout( mainPanel,BoxLayout.PAGE_AXIS));
        mainPanel.setPreferredSize(new Dimension(600,300));
        JTextArea screenData=new JTextArea("datadd");
        JScrollPane screenScrollPanel=new JScrollPane(screenData);
        screenScrollPanel.setBounds(10,5,100,100);
        screenScrollPanel.setMaximumSize(new Dimension(500,200));
        JButton startButton=new JButton("Start");
        JButton stopButton=new JButton("Stop");
        JTextField lenghtOfProductionBelt=new JTextField();
        lenghtOfProductionBelt.setMaximumSize(new Dimension(100,50));
        JTextField sleepData=new JTextField();
        sleepData.setMaximumSize(new Dimension(100,50));
        mainPanel.add(screenScrollPanel);
        mainPanel.add(startButton);
        mainPanel.add(stopButton);
        mainPanel.add(lenghtOfProductionBelt);
        mainPanel.add(sleepData);


        mainPanel.setVisible(true);

        //add info for user label;
        userInfo=new JLabel();
        userInfo.setText("Jacek Ciesielski");
        frame.getContentPane().add(mainPanel,BorderLayout.CENTER);
        frame.getContentPane().add(userInfo,BorderLayout.PAGE_END);


        frame.pack();
        frame.setVisible(true);
    }
}
