package Netriuscode.company.application.model;

import org.omg.PortableServer.THREAD_POLICY_ID;

public class Demo  {
    private char[] podane;
    private int i;

    public Demo(char[] podane) {
        this.podane = podane;

    }

    public void printCount() {
        try {
            if(i<podane.length){
                i++;
                System.out.println(podane[i]);
                Thread.sleep(100);
            }else{
                Thread.currentThread().interrupt();
            }

        } catch (Exception e) {
            System.out.println("Thread  interrupted.");
        }
    }
}
