package Netriuscode.company.application.model;

public class Robot extends Thread {
    private String name;
    public Thread t;
     Demo PD;
     public Robot(String name, Demo pd) {
        this.name = name;
        this.t=new Thread(this, name);
        t.start();
        this.PD =pd;
    }

    @Override
    public void run() {
        synchronized(PD) {

            PD.printCount();
        }

    }
}
